// soal 1
console.log('soal 1');

console.log('LOOPING PERTAMA');

var deret = 2;
var jumlah = 0;
while(jumlah < 20) {
  jumlah += deret;
  console.log(jumlah + ' - I love coding');
}

console.log('LOOPING KEDUA');

var deret = 2;
var jumlah = 22;
while(jumlah > 2) {
  jumlah -= deret;
  console.log(jumlah + ' - I will become a frontend developer');
}

// soal 2

console.log('soal 2');

for(var angka = 1; angka <= 20; angka++){
	if (angka % 2 === 0) {
        console.log(angka + " - Berkualitas");   
    }
	else if (angka % 3 === 0) {
        console.log(angka + " - I Love Coding");   
    }
    else {
        console.log(angka + " - Santai");
    }
}

// soal 3

console.log('soal 3');

var number = 0;
while(number < 7) {
  number++;
  var hashtag = '#'
  console.log(hashtag.repeat(number)); 
}

// soal 4

console.log('soal 4');

var kalimat = "saya sangat senang belajar javascript";
var kata = kalimat.split(" ")
console.log(kata)

// soal 5

console.log('soal 5')

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
var daftarBuahBaru = daftarBuah.join("\n");
console.log(daftarBuahBaru);