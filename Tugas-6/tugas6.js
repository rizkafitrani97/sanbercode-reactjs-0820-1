//soal 1
console.log("soal 1")

const luasLingkaran = (radius) => {
    return Math.PI * radius * radius
}

let luasBaru = luasLingkaran(10)
console.log("luas lingkaran dengan radius 10 adalah " + luasBaru)

let kelilingLingkaran = (radius) => {
    return 2 * Math.PI * radius
}

const kelilingBaru = kelilingLingkaran(10)
console.log("keliling lingkaran dengan radius 10 adalah " + kelilingBaru)

//soal 2
console.log("soal 2")

let kalimat = ""

const kalimatBaru = (kataPertama, kataKedua, kataKetiga, kataKeempat, kataKelima) => {
    return `${kalimat}${kataPertama} ${kataKedua} ${kataKetiga} ${kataKeempat} ${kataKelima}`
}

let theString = kalimatBaru("saya", "adalah", "seorang", "frontend", "developer")
console.log(theString)

//soal 3
console.log("soal 3")

const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(`${firstName} ${lastName}`)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

//soal 4
console.log("soal 4")

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation} = newObject

console.log(firstName, lastName, destination, occupation)

//soal 5
console.log("soal 5")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)