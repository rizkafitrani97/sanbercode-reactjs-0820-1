//soal 1
console.log("soal 1")

//release 1
console.log("release 1")

class Animal {
    // Code class di sini
	    constructor(name) {
	    	this._name = name
	    	this._legs = 4
	    	this._cold_blooded = false
		}
	  	get name() {
	    	return this._name;
  		}
  		get legs() {
	    	return this._legs;
  		}
  		get cold_blooded() {
	    	return this._cold_blooded;
  		}
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//release 2
console.log("release 2")

class Ape extends Animal {
  constructor(name) {
    super(name);
    this._legs = 2;
  }
  yell() {
    return "Auooo";
  }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"

class Frog extends Animal {
  constructor(name) {
    super(name);
  }
  jump() {
    return "hop hop";
  }
}

var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop"

//soal 2
console.log("soal 2")

class Clock {
  constructor({template}) {
  	var timer;
  }
	render() {
	var date = new Date();
    this.hours = date.getHours();
    if (this.hours < 10) this.hours = '0' + this.hours;

    this.mins = date.getMinutes();
    if (this.mins < 10) this.mins = '0' + this.mins;

    this.secs = date.getSeconds();
    if (this.secs < 10) this.secs = '0' + this.secs;
	
	this.output = 'h:m:s'
      .replace('h', this.hours)
      .replace('m', this.mins)
      .replace('s', this.secs);
      console.log (this.output);
  }
    stop() {
    return clearInterval(timer);
  };

  start() {
  	return setInterval(this.render, 1000);
  }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();