// soal 1
console.log('soal 1');

/*
    Tulis code function di sini
*/
function halo() {
  return "Halo Sanbers!"
}
 
console.log(halo()) // "Halo Sanbers!" 

// soal 2
console.log('soal 2');

/*
    Tulis code function di sini
*/
function kalikan(angkaPertama, angkaKedua) {   
   return angkaPertama * angkaKedua 
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2);
console.log(hasilKali) // 48

// soal 3
console.log('soal 3');

/* 
    Tulis kode function di sini
*/

function introduce(name, age, address, hobby) {
	return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
}
 
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"

// soal 4
console.log('soal 4');

var arrayDaftarPeserta = {
    nama: "Asep",
    "jenis kelamin": "laki-laki",
    hobi: "baca buku",
    "tahun lahir": 1992
}

console.log(arrayDaftarPeserta);

// soal 5
console.log('soal 5');

var buah = [{
  nama: "strawberry",
  warna: "merah",
  "ada bijinya": "tidak",
  harga: 9000
  }, {
  nama: "jeruk",
  warna: "oranye",
  "ada bijinya": "ada",
  harga: 8000
  }, {
  nama: "Semangka",
  warna: "Hijau & Merah",
  "ada bijinya": "ada",
  harga: 10000
  }, {
  nama: "Pisang",
  warna: "Kuning",
  "ada bijinya": "tidak",
  harga: 5000
  }]

console.log(buah[0])

// soal 6
console.log('soal 6');

var dataFilm = []


var dataFilm = []

function film(x, y, z) {
	var film = { 
	judul: x,
	genre: y,
  	"tahun produksi": z}
  return film
}
 
var dataFilm = film("Titanic", "Romantis", "1997")
console.log(dataFilm)